import numpy as np


def lin_search(arr,x):
    
    occur = 0
    pos = None
    for idx, val in enumerate(arr):
        #print(val)
        if val == x:
            occur = 1
            pos = idx

    return occur,pos


arr = np.random.randint(1,10, size=10)
occur,pos = lin_search(arr,5)

print(occur)
print(pos)


